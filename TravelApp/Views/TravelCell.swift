import UIKit

class TravelCell: UITableViewCell {

    @IBOutlet weak var travelNameLabel: UILabel!
    
    @IBOutlet weak var travelSubtitleLabel: UILabel!
    
    @IBOutlet weak var star1: UIImageView!
    @IBOutlet weak var star2: UIImageView!
    @IBOutlet weak var star3: UIImageView!
    @IBOutlet weak var star4: UIImageView!
    @IBOutlet weak var star5: UIImageView!
   
    @IBOutlet weak var travelView: UIView!
    
    //@IBOutlet var stars: Array<UIImageView>?
}
