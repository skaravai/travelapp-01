//
//  UIView+CoreKit.swift
//  TravelApp
//
//  Created by Karavai on 12/06/2019.
//  Copyright © 2019 Karavai. All rights reserved.
//

import UIKit

extension UIView {
    
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
}
