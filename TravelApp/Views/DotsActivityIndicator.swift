import UIKit

enum AnimationKeys {
    static let group = "scaleGroupAnimation"
}
enum AnimationsConstans {
    static let dotScale: CGFloat = 1.5
    static let scaleUpDuration: CFTimeInterval = 0.2
    static let scaleDownDuration: CFTimeInterval = 0.2
    static let offset: CFTimeInterval = 0.1
}

@IBDesignable
class DotsActivityIndicator: UIView {

    var dots: [UIView] = []
    @IBInspectable
    var dotsCount: Int = 3 {
        didSet {
            removeDots()
            configureDots()
            setNeedsLayout()
        }
    }
    
    @IBInspectable
    var dotRadius: CGFloat = 10 {
        didSet {
            for dot in dots {
                configureDotSize(dot)
            }
            setNeedsLayout()
        }
    }
    
    @IBInspectable
    var dotSpacing: CGFloat = 15
    
    override var tintColor: UIColor! {
        didSet {
            for dot in dots {
                configureDotColor(dot)
            }
            setNeedsLayout()
        }
    }
    
    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureDots()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureDots()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let offset = (frame.size.width - (2 * dotRadius + dotSpacing) * CGFloat(dotsCount) - dotSpacing)/2
        for i in 0..<dots.count {
            let y = frame.size.height / 2 - dotRadius
            let x = offset + (2 * dotRadius + dotSpacing) * CGFloat(i)
            let dot = dots[i]
            dot.frame.origin = CGPoint.init(x: x, y: y)
        }
    }
    
    //MARK: - Private
    func configureDots() {
        for i in 0..<dotsCount {
            let dot = UIView()
            dot.backgroundColor = .red
            configureDotSize(dot)
            configureDotColor(dot)
            dots.append(dot)
            addSubview(dot)
        }
        startAnimation()
    }
    
    func removeDots() {
        for dot in dots {
            dot.removeFromSuperview()
        }
        dots.removeAll()
    }
    
    func configureDotSize(_ dot: UIView) {
        dot.frame = CGRect(x: 0, y: 0, width: dotRadius * 2, height: dotRadius * 2)
        dot.cornerRadius = dotRadius
    }
    
    func configureDotColor(_ dot: UIView) {
        dot.backgroundColor = tintColor
    }
    
    func scaleAnimation(delay: CFTimeInterval) -> CAAnimationGroup {
        let scaleUp = CABasicAnimation(keyPath: "transform.scale")
        scaleUp.duration = AnimationsConstans.scaleUpDuration
        scaleUp.fromValue = 1
        scaleUp.toValue = AnimationsConstans.dotScale
        scaleUp.beginTime = delay
        
        let scaleDown = CABasicAnimation(keyPath: "transform.scale")
        scaleDown.duration = AnimationsConstans.scaleDownDuration
        scaleDown.fromValue = AnimationsConstans.dotScale
        scaleDown.toValue = 1
        scaleDown.beginTime = delay + scaleUp.duration
        
        let group = CAAnimationGroup()
        group.animations = [scaleUp, scaleDown]
        group.repeatCount = .infinity
        group.duration = (AnimationsConstans.scaleDownDuration + AnimationsConstans.scaleUpDuration) * Double(dotsCount)
        
        return group
    }
    
    //MARK: - Public
    func startAnimation() {
        var offset: CFTimeInterval = 0
        for dot in dots {
            dot.layer.removeAnimation(forKey: AnimationKeys.group)
            let animationGroup = scaleAnimation(delay: offset)
            dot.layer.add(animationGroup, forKey: AnimationKeys.group)
            offset = offset + AnimationsConstans.offset
        }
    }

    func stopAnimation() {
        for dot in dots {
            dot.layer.removeAnimation(forKey: "AnimationKeys.group")
        }
    }
}
