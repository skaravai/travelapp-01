//
//  SpendMoneyViewController.swift
//  TravelApp
//
//  Created by Karavai on 10/04/2019.
//  Copyright © 2019 Karavai. All rights reserved.
//

import UIKit

class SpendMoneyViewController: UIViewController {
    var delegate: CreateStopViewController? 
    
    @IBOutlet weak var spendMoneyTextField: UITextField!
    
    @IBAction func safeMoneyButtonClicked(_ sender: UIButton) {
        if let text = spendMoneyTextField.text{
           delegate?.userSpentMoney(text)
        }
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func selectedCurrency(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            delegate?.changeSelectedCurrency("$")
        } else if sender.selectedSegmentIndex == 1 {
            delegate?.changeSelectedCurrency("€")
        } else if sender.selectedSegmentIndex == 2 {
            delegate?.changeSelectedCurrency("₽")
        }

    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
  
    
    
}
