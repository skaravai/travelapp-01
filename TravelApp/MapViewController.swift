//
//  MapViewController.swift
//  TravelApp
//
//  Created by Karavai on 24/04/2019.
//  Copyright © 2019 Karavai. All rights reserved.
//

import UIKit
import MapKit

protocol MapViewControllerDelegate {
    func mapControllerDidSelectPoint(_ point: MKPointAnnotation)
}

class MapViewController: UIViewController {
    //MARK: - Properties (свойства)
    var array: [MKPointAnnotation]?
    var delegate: MapViewControllerDelegate?
    
    //MARK: - Outlets
    @IBOutlet weak var mapView: MKMapView!
    
    //MARK: - Actions
    @IBAction func saveClicked(_ sender: Any) {
        if let selectedPoint = mapView.annotations.first as? MKPointAnnotation {
            delegate?.mapControllerDidSelectPoint(selectedPoint)
        }
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func mapClicked(_ recognizer: UITapGestureRecognizer) {
        let point = recognizer.location(in: mapView)
        let tapPoint = mapView.convert(point, toCoordinateFrom: mapView)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = tapPoint
        
        mapView.removeAnnotations(mapView.annotations)
        mapView.addAnnotation(annotation)
    }
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //navigationController?.isNavigationBarHidden = true
        if let array = array {
            for point in array {
                mapView.addAnnotation(point) //
            }
    
        }
    }

}

