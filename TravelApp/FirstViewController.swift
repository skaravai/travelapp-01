//
//  FirstViewController.swift
//  TravelApp
//
//  Created by Karavai on 25/04/2019.
//  Copyright © 2019 Karavai. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
    
    @IBOutlet weak var someTextField: UITextField!
    @IBOutlet weak var comebackLabel: UILabel!
    
    @IBAction func goButtonClicked(_ sender: Any) {
        
        let newVC = (storyboard?.instantiateViewController(withIdentifier: "TwoViewController"))
            as? TwoViewController
        newVC?.delegate = self
        //newVC?.textXX = "то что хочу передать" 
        navigationController?.pushViewController(newVC!, animated: true)
    }
    
    func dannieKotorieBudutVozvrasheni(_ data1: String) {
        comebackLabel.text = data1
    }
    
    override func viewWillAppear(_ animated: Bool) {   // функция, которая используется перед открытием экрана
        super.viewWillAppear(animated)
        someTextField.text = ""
    }
}

