//
//  WelcomeViewController.swift
//  TravelApp
//
//  Created by Karavai on 17/04/2019.
//  Copyright © 2019 Karavai. All rights reserved.
//

import UIKit
import Firebase

class WelcomeViewController: UIViewController {
    
    @IBAction func loginClicked(_ sender: Any) {
        let loginController = UIViewController.getFromStoryboard(withId: "LoginViewController")
        navigationController?.pushViewController(loginController!, animated: true)
    //    let loginController = UIViewController.getFromStoryboard(withId: "LoginViewController")
//        present(loginController!, animated: true, completion: nil)
    }
    @IBAction func registerClicked(_ sender: Any) {
        let loginController = UIViewController.getFromStoryboard(withId: "CreateAccountViewController")
        navigationController?.pushViewController(loginController!, animated: true)
    }
   
    
    @IBOutlet weak var moreWaysToLoginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        
        let remoteConfig = RemoteConfig.remoteConfig()
        remoteConfig.fetch { (status, error) in
            remoteConfig.activateFetched()
            if let buttonTitle = remoteConfig["button_title"].stringValue {
                self.moreWaysToLoginButton.setTitle(buttonTitle, for: .normal)
            }
            
            let isButtonHidden = remoteConfig["button_hidden"].boolValue
            self.moreWaysToLoginButton.isHidden = isButtonHidden
        }
        
    }
}


