import UIKit

class TwoViewController: UIViewController {
    
    var delegate: FirstViewController?
    var textXX = "" // создаем переменную для временного хранения текста
    @IBOutlet weak var someLabel: UILabel!
    @IBOutlet weak var secondTextField: UITextField!
    
    override func viewDidLoad() {
        someLabel.text = delegate?.someTextField.text //textXX //присваиваем значение textXX в someLabel.text
        super.viewDidLoad()
    }
    
    @IBAction func goBackClicked(_ sender: Any) {
        if let text = secondTextField.text {
            delegate?.dannieKotorieBudutVozvrasheni(text) // извлекаем опционал, если в secondTextField есть текст, то передаем его, как данные (data1) в функцию делегата (1ого VC) dannieKotorieBudutVozvrasheni
        }
        navigationController?.popViewController(animated: true)
    }
}





