import UIKit

class StopsListViewController: UIViewController {

    //var travel: Travel!
    var realmTravel: RealmTravel!
    var delegate: TravelsListViewController?
   
    //MARK: - Outlets
    //@IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableView2: UITableView!
    @IBOutlet weak var noStopsLabel: UILabel!
    @IBOutlet weak var countryNameLabel: UILabel!
    
    //MARK: - Actions
    @IBAction func backClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func newStopClicked(_ sender: Any) {
        let createVC = UIViewController.getFromStoryboard(withId: "CreateStopViewController") as! CreateStopViewController
        createVC.delegate = self
        createVC.stopDidCreateClosure = { stop in
            DatabaseManager.instance.addStop(stop, to: self.realmTravel)
            DatabaseManager.instance.saveObjects([self.realmTravel], types: RealmTravel.self)
            //DatabaseManager.instance.saveToDatabase(travels: [self.realmTravel]) //обновляем информацию о Travels в БД
        }
        navigationController?.pushViewController(createVC, animated: true)
    }
    
    //MARK: - Properties
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // изменияем заголовок экрана на название страны, созданную на предыдущем экране
        countryNameLabel.text = realmTravel.name
        
        //при открытии проверяем не пустой ли массив остановок, если нет, то скрываем Label
        if self.realmTravel.stops.count != 0 {
            self.noStopsLabel.isHidden = true
        }
    }
    //используем для перезагрузки данных в ячейке после правок
    override func viewDidAppear(_ animated: Bool) {
        tableView2.reloadData()
        if self.realmTravel.stops.count != 0 {
            self.noStopsLabel.isHidden = true
        }
    }
    
}

extension StopsListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return realmTravel.stops.count // количество ячеек в таблице будет равно количеству элементов в массиве stops
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StopCell", for: indexPath) as! StopCell // функция означает: предоставь возможность переиспользовать ячейку таблицы с таким то ID, метод cellforRaw
        let stop = realmTravel.stops[indexPath.row] //
        
        //тень для View
        cell.stopView.layer.shadowColor = UIColor.gray.cgColor
        cell.stopView.layer.shadowOpacity = 1
        cell.stopView.layer.shadowOffset = .zero
        cell.stopView.layer.shadowRadius = 5
        
        cell.stopView.layer.shouldRasterize = true
        cell.stopView.layer.rasterizationScale = UIScreen.main.scale
        
        //значение лейблов в TableViewCell
        cell.stopTitleLabel.text = stop.name
        cell.stopSubTitleLabel.text = stop.desc
        cell.spendSummLabel.text = stop.spendMoney
        cell.currencyLabel.text = stop.currency
        
        cell.selectionStyle = .none //отключить подсветку ячейки при тапе по ней
        
        switch stop.rating {
        case 1:
            cell.star1.isHighlighted = true
            break
        case 2:
            cell.star1.isHighlighted = true
            cell.star2.isHighlighted = true
            break
        case 3:
            cell.star1.isHighlighted = true
            cell.star2.isHighlighted = true
            cell.star3.isHighlighted = true
            break
        case 4:
            cell.star1.isHighlighted = true
            cell.star2.isHighlighted = true
            cell.star3.isHighlighted = true
            cell.star4.isHighlighted = true
            break
        case 5:
            cell.star1.isHighlighted = true
            cell.star2.isHighlighted = true
            cell.star3.isHighlighted = true
            cell.star4.isHighlighted = true
            cell.star5.isHighlighted = true
            break
        default:
            cell.star1.isHighlighted = false
            cell.star2.isHighlighted = false
            cell.star3.isHighlighted = false
            cell.star4.isHighlighted = false
            cell.star5.isHighlighted = false
        }
        
        let autoImage = UIImage(named: "Group 3")
        let plainImage = UIImage(named: "plane")
        let trainImage = UIImage(named: "Group 2")
        
        switch stop.transportType {
        case .plane:
            cell.transportImage.image = UIImage?(plainImage!)
            break
        case .auto:
            cell.transportImage.image = UIImage?(autoImage!)
            break
        case .train:
            cell.transportImage.image = UIImage?(trainImage!)
            break
        default:
            cell.transportImage.image = UIImage?(plainImage!)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let createStopVC = UIViewController.getFromStoryboard(withId: "CreateStopViewController") as! CreateStopViewController
        createStopVC.stop = realmTravel.stops[indexPath.row]
        navigationController?.pushViewController(createStopVC, animated: true)
    }
}

extension StopsListViewController: CreateStopViewControllerDelegate {
    func createStopControllerDidCreateStop(_ stop: RealmStop) {
        realmTravel.stops.append(stop)
        tableView2.reloadData()
        
        // проверяем пустой ли массив stops, если нет, то убираем Label
        if self.realmTravel.stops.isEmpty {
            self.noStopsLabel.isHidden = false}
        else {
            self.noStopsLabel.isHidden = true
        }
    }
    
    // удаление выбранной ячейки свайпом справа-налево
    func tableView(_ tableView2: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete {
            let stop = realmTravel.stops[indexPath.row]
            DatabaseManager.instance.deleteObject(stop)
            //DatabaseManager.instance.delete(stop: stop)
            tableView2.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
        }
    }
}
