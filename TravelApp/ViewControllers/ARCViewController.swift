
import UIKit

protocol  ARCViewControllerDelegate {
    
}

class ARCViewController: UIViewController {
    var user: ARCUser?
    var delegate: ARCViewControllerDelegate?
    var closure: ( () -> () )?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let user1 = ARCUser(name: "Serj")
//        user = user1
//
//        var users: [ARCUser] = []
//        for i in 0..<10 {
//            let user = ARCUser(name: "Serj1")
//            users.append(user)
//        }
        
//        let serj = ARCUser(name: "Serj")
//        let puxa = ARCAnimal(name: "Puxa")
//        serj.animal = puxa
//        puxa.owner = serj
        
        // пишем weak self во всех замыканиях
//        closure = { [weak self] in
//            print(self?.delegate)
//        }
//
        ApiManager.instance.getUsers { [weak self] (users) in
            print(self)
        }
    }
    
    deinit {
        print(">>> Экран удаляется")
    }
   
}

class ARCUser {
    let name: String
    var animal: ARCAnimal?
    init(name: String) {
        self.name = name
        print(">>> Человек с именем \(name) создается")
    }
    
    deinit {
        print(">>> Человек \(name) удаляется")
    }
}

class ARCAnimal {
    let name: String
    weak var owner: ARCUser?
    init(name: String) {
        self.name = name
        print(">>> Животное с именем \(name) создается")
    }
    
    deinit {
        print(">>> Животное \(name) удаляется")
    }
}
