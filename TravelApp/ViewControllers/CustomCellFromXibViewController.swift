import UIKit

class CustomCellFromXibViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let xib = UINib.init(nibName: "CustomXibCell", bundle: nil)
        tableView.register(xib, forCellReuseIdentifier: "cell")
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "UserAvatarDidChanged"), object: nil, queue: nil) { (notification) in
            print("Случилось событие!!! \(notification.object)")
            self.nameLabel.text = notification.object as? String
        }
    }
}

extension CustomCellFromXibViewController: UITabBarDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        return cell
    }
    
    
}
