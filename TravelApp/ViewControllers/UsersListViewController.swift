//
//  UsersListViewController.swift
//  TravelApp
//
//  Created by Karavai on 17/05/2019.
//  Copyright © 2019 Karavai. All rights reserved.
//

import UIKit

class UsersListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var users: [RealmUser] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        users = DatabaseManager.instance.getObjects(classType: RealmUser.self)
//        ApiManager.instance.getUsers { (userFromServer) in
//            for user in userFromServer {
//                let realmUser = RealmUser()
//                realmUser.id = user.id
//                realmUser.name = user.name ?? ""
//                realmUser.username = user.username ?? ""
//                realmUser.email = user.email ?? ""
//                self.users.append(realmUser)
//            }
//            DatabaseManager.instance.saveToDatabase(users: self.users)
//            self.tableView.reloadData()
//        }
    }
}


extension UsersListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as! UserCell
        if users.count > 0 {
        let user = users[indexPath.row]
        cell.userNameLabel.text = user.name
        cell.nameLabel.text = user.username
        cell.emailLabel.text = user.email
        }
        return cell
    }
}
