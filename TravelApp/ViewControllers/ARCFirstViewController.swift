//
//  ARCFirstViewController.swift
//  TravelApp
//
//  Created by Karavai on 26/06/2019.
//  Copyright © 2019 Karavai. All rights reserved.
//

import UIKit

class ARCFirstViewController: UIViewController {
    weak var arcController: ARCViewController?
    override func viewDidLoad() {
        super.viewDidLoad()

      
    }
    
    @IBAction func buttonClicked(_ sender: Any) {
        let vc = UIViewController.getFromStoryboard(withId: "ARCViewController") as! ARCViewController
        vc.delegate = self
        arcController = vc
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension ARCFirstViewController: ARCViewControllerDelegate {
    
}
