import UIKit

class AnimationsViewController: UIViewController {

    @IBOutlet weak var firstSquare: UIView!
    
    @IBOutlet weak var secondSquare: UIView!
    
    @IBOutlet weak var secondSquareTopConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func animateButtonClicked(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: {
            self.firstSquare.frame.origin.y += 90
            self.firstSquare.layer.cornerRadius = self.firstSquare.frame.size.width / 2
            self.firstSquare.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            self.firstSquare.backgroundColor = .blue
        }, completion: { success in
            
        })
    }
    
    
    @IBAction func secondButtonClicked(_ sender: Any) {
        secondSquareTopConstraint.constant = 300
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    @IBAction func thirdButtonClicked(_ sender: Any) {
        let scale = CABasicAnimation(keyPath: "transform.scale")
        scale.duration = 0.5
        scale.fromValue = 1
        scale.toValue = 1.5
        scale.repeatCount = .infinity
        scale.autoreverses = true
        secondSquare.layer.add(scale, forKey: "qeqeeryey")
    }
    
}
