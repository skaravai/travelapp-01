//
//  ViewController.swift
//  TravelApp
//
//  Created by Karavai on 03/04/2019.
//  Copyright © 2019 Karavai. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "1Test" {
            if let secondVC = segue.destination as? SecondViewController {
                secondVC.view.backgroundColor = getCurrentColor()
            }
        }
    }
    
    func getCurrentColor() -> UIColor {
        var currentColor = UIColor.clear
        if segmentedControl.selectedSegmentIndex == 0 {
            currentColor = .red
        } else {
            currentColor = .blue
        }
        return currentColor
    }
}

