//
//  NotificationCenterViewController.swift
//  TravelApp
//
//  Created by Karavai on 14/06/2019.
//  Copyright © 2019 Karavai. All rights reserved.
//

import UIKit

class NotificationCenterViewController: UIViewController {

    @IBOutlet weak var buttonClicked: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func buttonClicked(_ sender: Any) {
        let notification = NSNotification.Name(rawValue: "UserAvatarDidChanged")
        let object = "qaddadd"
        NotificationCenter.default.post(name: notification, object: object)
        dismiss(animated: true, completion: nil)
    }

}
