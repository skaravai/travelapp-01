//
//  RealmNotificationsExampleSecondViewController.swift
//  TravelApp
//
//  Created by Karavai on 05/06/2019.
//  Copyright © 2019 Karavai. All rights reserved.
//

import UIKit

class RealmNotificationsExampleSecondViewController: UIViewController {

    var travel: RealmTravel?
    
    @IBOutlet weak var textField: UITextField!
    
    @IBAction func textFieldDidChange(_ sender: Any) {
        DatabaseManager.instance.updateTravel(travel!, withName: textField.text!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        travel = DatabaseManager.instance.getTravelFromDatabase(withId: "XXXXXX")

    }
}

//extension RealmNotificationsExampleSecondViewController: UITextViewDelegate {
//    func textViewDidChange(_ textView: UITextView) {
//        <#code#>
//    }
//}
