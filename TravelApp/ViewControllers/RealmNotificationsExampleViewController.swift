//
//  RealmNotificationsExampleViewController.swift
//  TravelApp
//
//  Created by Karavai on 05/06/2019.
//  Copyright © 2019 Karavai. All rights reserved.
//

import UIKit
import RealmSwift

class RealmNotificationsExampleViewController: UIViewController {
    
    var travel: RealmTravel?
    var notificationToken: NotificationToken? = nil
    
    @IBOutlet weak var resultLabel: UILabel!
    
    @IBAction func createTravel(_ sender: Any) {
        travel = RealmTravel()
        travel?.id = "XXXXXX"
        travel?.name = "adafdhad"
        DatabaseManager.instance.saveToDatabase(travels: [travel!])
        notificationToken = travel?.observe({ (change) in
            switch change {
            case .change(let properties):
                for property in properties {
                    if property.name == "name" {
                        self.resultLabel.text = property.newValue as? String
                    }
                }
            case .error(let error):
                print("An error occurred: \(error)")
            case .deleted:
                print("The object was deleted.")
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
}
