import Foundation
import RealmSwift

class RealmStop: Object {
    @objc dynamic var id: String = UUID().uuidString
    @objc dynamic var name: String = ""
    @objc dynamic var rating: Int = 1
    @objc dynamic var location: String = ""
    @objc dynamic var spendMoney: String = ""
    @objc dynamic var currency: String = "$"
    @objc dynamic var transportType: TransportType = .plane
    @objc dynamic var desc: String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}


