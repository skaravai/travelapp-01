import UIKit
import RealmSwift

class RealmUser: Object {
    @objc dynamic var id: String = ""
    @objc dynamic var name: String?
    @objc dynamic var username: String?
    @objc dynamic var email: String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
