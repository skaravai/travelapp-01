//
//  RealmTravel.swift
//  TravelApp
//
//  Created by Karavai on 31/05/2019.
//  Copyright © 2019 Karavai. All rights reserved.
//

import Foundation
import RealmSwift

class RealmTravel: Object {
    @objc dynamic var id: String = UUID().uuidString //автоматическая генерация рандомного ID для каждого путешествия
    @objc dynamic var name: String = ""
    @objc dynamic var desc: String = ""
    @objc dynamic var rating: String = ""
    let stops = List<RealmStop>() //так объявляется массив остановок в БД Realm
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func averageRate () -> Int {
        if stops.count == 0 {
            return 0
        }
        var averageRate = 0
        var sum = 0
        
        stops.forEach { stop in
            sum += stop.rating
        }
        
        averageRate = sum/stops.count
        
        return averageRate
    }
}
