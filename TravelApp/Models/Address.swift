//
//  Address.swift
//  TravelApp
//
//  Created by Karavai on 17/05/2019.
//  Copyright © 2019 Karavai. All rights reserved.
//

import Foundation

class Address {
    var street: String?
    var suite: String?
    var city: String?
    var zipcode: String?
    var geo: Geo?
    
}

