//
//  PrintInfo.swift
//  TravelApp
//
//  Created by Karavai on 18/05/2019.
//  Copyright © 2019 Karavai. All rights reserved.
//

import Foundation

class PrintInfo {
   public func print(users: [User], addresses: [Address], geos: [Geo], companies: [Company]) -> String {
        
        var result = ""
       
        users.forEach { user in
            result += "'id': \(user.id)\n name: \(user.name)\n,username: \(user.username)\n,email: \(user.email)\n"
        }
        
        addresses.forEach { address in
            result += "'address':\n street: \(address.street)\n,suite: \(address.suite)\n,city: \(address.city)\n,zipcode: \(address.zipcode)\n"
        }
        
        geos.forEach { geo in
            result += "'geo':\n lat: \(geo.latitude),\n lon: \(geo.longitude)\n"
        }
        
        users.forEach { user in
            result += "\n phone: \(user.phone),\n website: \(user.website)\n"
        }
        
        companies.forEach { company in
            result += "name: \(company.name)\n,catchPhrase: \(company.catchPhrase),\n bs: \(company.bs)\n"
        }
    
    //Swift.print(result)
        return result
    }
}
