//
//  Geo.swift
//  TravelApp
//
//  Created by Karavai on 18/05/2019.
//  Copyright © 2019 Karavai. All rights reserved.
//

import Foundation

class Geo {
    var latitude: String = ""
    var longitude: String = ""
}
