//
//  Company.swift
//  TravelApp
//
//  Created by Karavai on 17/05/2019.
//  Copyright © 2019 Karavai. All rights reserved.
//

import Foundation

class Company {
    var name: String = ""
    var catchPhrase: String = ""
    var bs: String = ""
}
