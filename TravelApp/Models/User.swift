//
//  User.swift
//  TravelApp
//
//  Created by Karavai on 17/05/2019.
//  Copyright © 2019 Karavai. All rights reserved.
//

import Foundation

class User: CustomStringConvertible {
    var id: String = ""
    var name: String?
    var username: String?
    var email: String?
    var phone: String?
    var website: String?
    var address: Address?
    var company: Company?
    var description: String {
        return """
        id: \(id)
        name: \(name)
        username: \(username)
        email: \(email)
        phone: \(phone)
        address: \(address?.geo)
        company: \(company?.bs)
        
        """
    }
    
    func printInfo() {
//        print("""
////            name: \(name)
////            company: \(company?.bs)
//              """)
    }
}
