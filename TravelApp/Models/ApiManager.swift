//
//  ApiManager.swift
//  TravelApp
//
//  Created by Karavai on 17/05/2019.
//  Copyright © 2019 Karavai. All rights reserved.
//

//https://jsonplaceholder.typicode.com/users

import Foundation
import Alamofire

class ApiManager {
    static var instance = ApiManager() //синглтон
    private enum Constants {
        static let baseURL = "https://jsonplaceholder.typicode.com"
    }
    
    private enum EndPoints {
        static let users = "/users"
    }
    
    func getUsers(onComplete: @escaping ([User]) -> Void) {
        let urlString = Constants.baseURL + EndPoints.users
        AF.request(urlString, method: .get, parameters: [:]).responseJSON { (response) in
            switch response.result {
            case .success(let data):
                //print(data)
                if let arrayUsers = data as? Array<Dictionary<String, Any>> {
                    var users: [User] = []
                    var addresses: [Address] = []
                    var geos: [Geo] = []
                    var companies: [Company] = []
                    for userDict in arrayUsers {
                        let user = User() //создали модель класса юзер
                        user.id = "\(userDict["id"] as? Int ?? 0)"
                        user.name = userDict["name"] as? String
                        user.username = userDict["username"] as? String
                        user.email = userDict["email"] as? String
                        //print(userDict)
                        if let addressDict = userDict["address"] as? Dictionary<String, Any> {
                            let address = Address()
                            address.street = addressDict["street"] as? String ?? ""
                            address.suite = addressDict["suite"] as? String
                            address.city = addressDict["city"] as? String
                            address.zipcode = addressDict["zipcode"] as? String
                            user.address = address
                            if let geoDict = addressDict["geo"] as? Dictionary<String, Any> {
                                let geo = Geo()
                                geo.latitude = geoDict["lat"] as? String ?? ""
                                geo.longitude = geoDict["lon"] as? String ?? ""
                                address.geo = geo
                                geos.append(geo) // ???
                            }
                            addresses.append(address) // ???
                        }
                        user.phone = userDict["phone"] as? String
                        user.website = userDict["website"] as? String
                        users.append(user)
                        if let companyDict = userDict["company"] as? [String:Any] {
                            let company = Company()
                            company.name = companyDict["name"] as? String ?? ""
                            company.catchPhrase = companyDict["catchPhrase"] as? String ?? ""
                            company.bs = companyDict["bs"] as? String ?? ""
                            user.company = company
                            companies.append(company) // ???
                        }
                    }
                    onComplete(users)
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func printInfo () -> String {
        
        let users: [User] = []
        let addresses: [Address] = []
        let geos: [Geo] = []
        let companies: [Company] = []
        
        var printInfo = ""
        if users.count == 0 {
            return ""
        }
        
        users.forEach { user in
            printInfo += "'id': \(user.id)\n name: \(user.name)\n,username: \(user.username)\n,email: \(user.email)\n"
        }
        
        if addresses.count == 0 {
            return ""
        }
        
        addresses.forEach { address in
            printInfo += "'address':\n street: \(address.street)\n,suite: \(address.suite)\n,city: \(address.city)\n, zipcode: \(address.zipcode)\n"
        }
        
        if geos.count == 0 {
            return ""
        }
        
        geos.forEach { geo in
            printInfo += "'geo':\n lat: \(geo.latitude),\n lon: \(geo.longitude)\n"
        }
        
        users.forEach { user in
            printInfo += "\n phone: \(user.phone),\n website: \(user.website)\n"
        }
        
        if companies.count == 0 {
            return ""
        }
        
        companies.forEach { company in
            printInfo += "name: \(company.name)\n,catchPhrase: \(company.catchPhrase),\n bs: \(company.bs)\n"
        }
    
        return printInfo
    }

}



//                    for user in 0...users.count {
//                        var user =
//                        print("'id': \(user.id)\n name: \(user.name),\n username: \(user.username),\n email: \(user.email)")
//                    }
//
//                    for address in addresses {
//                        print("'address':\n street: \(address.street),\n suite: \(address.suite),\n city: \(address.city), zipcode: \(address.zipcode)")
//                    }
//
//                    for user in users {
//                        print("\n phone: \(user.phone),\n website: \(user.website)  ")
//                    }
//
//                    for company in companies {
//                        print("\n'company': name: \(company.name),\n catchPhrase: \(company.catchPhrase),\n bs: \(company.bs)")
//                    }
