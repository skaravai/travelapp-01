import Foundation
import RealmSwift

class DatabaseManager {
    static var instance = DatabaseManager()
    
//    func save<T>(_ objects: [T]) {
//        let realm = try! Realm()
//        try! realm.write {
//            if let objects = objects as? [Object] {
//            realm.add(objects, update: true)
//            }
//        }
//    }
    
    //если наследовать Т от Object, то проверка не нужна.
    func saveObjects<T: Object, S>(_ objects: [T], types: S) {
        let realm = try! Realm()
        try! realm.write {
            realm.add(objects, update: true)
        }
    }
    
//    func saveObject<T: Object, S>(_ object: T, type: S) {
//        let realm = try! Realm()
//        try! realm.write {
//            realm.add(object, update: true)
//        }
//    }
    
    
    func getObjects<T: Object>(classType: T.Type) -> [T] {
        let realm = try! Realm()
        let result = realm.objects(T.self)
        
        return Array(result)
    }
    
    func deleteObject<T: Object>(_ object: T) {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(object)
        }
    }
    
    
//    func saveToDatabase(users: [RealmUser]) {
//        let realm = try! Realm()
//        try! realm.write {
//            realm.add(users, update: true)
//        }
//    }
//
    func saveToDatabase(travels: [RealmTravel]) {
        let realm = try! Realm()
        try! realm.write {
            realm.add(travels, update: true)
        }
    }
    
//    func getUsersFromDatabase() -> [RealmUser] {
//        let realm = try! Realm()
//        let usersResult = realm.objects(RealmUser.self)
//
//        var users: [RealmUser] = []
//        for realmUser in usersResult {
//            users.append(realmUser)
//        }
//        return users
//    }
    
//    func getTravelsFromDatabase() -> [RealmTravel] {
//        let realm = try! Realm()
//        let realmTravelsResult = realm.objects(RealmTravel.self)
//        return Array(realmTravelsResult)
//
//
//        //        var travels: [RealmTravel] = []
////        for realmTravel in realmTravelsResult {
////            let travel = RealmTravel()
////            travel.name = realmTravel.name
////            travel.desc = realmTravel.desc
////            travels.append(travel)
////        }
//
//    }
    
    func getTravelFromDatabase(withId id: String) -> RealmTravel? {
        let realm = try! Realm()
        let travel = realm.object(ofType: RealmTravel.self, forPrimaryKey: id)
        return travel
    }
    
    
    func updateTravel(_ travel: RealmTravel, withName name: String) {
        let realm = try! Realm()
        try! realm.write {
            travel.name = name
        }
    }
    
    func addStop(_ stop: RealmStop, to travel: RealmTravel) {
        let realm = try! Realm()
        try! realm.write {
            travel.stops.append(stop)
        }
    }
    
    func deleteTravelInDatabase(_ travel: RealmTravel){
        let realm = try! Realm()
        try! realm.write {
            realm.delete(travel)
        }
    }
    
    func delete(stop:RealmStop) {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(stop)
        }
    }
}
