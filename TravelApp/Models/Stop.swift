import RealmSwift

class Stop {
    var name: String = ""
    var rating: Int = 1 
    var location: String = "" //
    var spendMoney: String = "" // может сделать Int
    var currency: String = "$"
    var transportType: TransportType = .plane
    var desc: String = ""
    
    func printAll() {
        print ("Город: \(name) \nРейтинг: \(rating) \nГео-данные: \(location) \nПотраченная сумма: \(spendMoney)\nТранспорт: \(transportType) \nОписание поездки: \(desc) ")
    }
}

@objc enum TransportType: Int {
    case plane, train, auto
}
