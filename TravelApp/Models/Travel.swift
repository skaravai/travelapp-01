import RealmSwift

class Travel: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var desc: String = ""
    @objc dynamic var rating: String = ""
    var stops: [Stop] = []

    func averageRate () -> Int {
        if stops.count == 0 {
            return 0
        }
        var averageRate = 0
        var sum = 0

        stops.forEach { stop in
            sum += stop.rating
        }
        
        averageRate = sum/stops.count
        
        return averageRate
    }
}
