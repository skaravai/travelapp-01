//
//  FourthViewController.swift
//  TravelApp
//
//  Created by Karavai on 09/04/2019.
//  Copyright © 2019 Karavai. All rights reserved.
//

import UIKit

internal class FourthViewController : UIViewController {
    //MARK: - Outlets
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var redButton: UIButton!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var secondButton: UIButton!
    @IBOutlet weak var secondTextLabel: UILabel!
    @IBOutlet weak var labelForSlider: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var progress: UIProgressView!
    
    
    //MARK: - Actions
    @IBAction func redButtonClicked(_ sender: UIButton) {
        textLabel.text = "А ты не трус :)" //нажатие на кнопку изменит текст в лейбле
        redButton.setTitle("Бабах", for: .normal)
        redButton.isEnabled = false //после нажатия кнопка будет неактивной
        secondButton.isEnabled = false // КОСТЫЛЬ. при выполнение функции redButtonClicked деактивирует мне кнопку для Расшифровки
    }
    
    @IBAction func changeText(_  sender: UITextField) {
        let text = sender.text
        secondTextLabel.text = text
    } //выводит в лейбл расшифрованные введенные значения без нажания на кнопку "Расшифровать", для работы изменить тип Editing did Changed
    
    @IBAction func secondButtonClicked(_ sender: UIButton) {
        let text = textField.text
        secondTextLabel.text = text
    }
    
    @IBAction func changeColor(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            redButton.backgroundColor = .red
        } else if sender.selectedSegmentIndex == 1 {
            redButton.backgroundColor = .blue }
        else if sender.selectedSegmentIndex == 2 {
            redButton.backgroundColor = .green
        }
//        switch Colors.self {
//        case:           sender.selectedSegmentIndex == 0;
//        default:
//            redButton.
//        }
//  Сделать через case
    }
    
    @IBAction func changeSlider(_ sender: UISlider) {
        labelForSlider.text = String(Int(sender.value)) //преобразовали к типу String, т.к. в лейбл только строковое значение, перед этим преобразовав к Int, т.к. value будет числом)
        
    }
    
    @IBAction func changeSwitch(_ sender: UISwitch) {
        if sender.isOn {
            secondButton.isEnabled = true
            indicator.startAnimating()
            
        } else {
            secondButton.isEnabled = false
            indicator.stopAnimating()
        }
    }
    
    
    @IBAction func stepAction(_ sender: UIStepper) {
        progress.progress = Float(sender.value)
    }
    
}


import Foundation

enum Colors {
   case red, blue, green
}
