//
//  ForgotPasswordViewController.swift
//  TravelApp
//
//  Created by Karavai on 20/04/2019.
//  Copyright © 2019 Karavai. All rights reserved.
//

import UIKit
import Firebase

class ForgotPasswordViewController: UIViewController {
    
    @IBAction func backClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resetClicked(_ sender: Any) {
        Auth.auth().sendPasswordReset(withEmail: "1@test.com") { (error) in
            if error == nil {
                print("новый пароль отправлен")
            } else {
                print("новый пароль не отправлен")
            }
            
        }
        
    }
    
}
    


