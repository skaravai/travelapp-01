//
//  LoginViewController.swift
//  TravelApp
//
//  Created by Karavai on 17/04/2019.
//  Copyright © 2019 Karavai. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {
    @IBAction func backClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func forgotPasswordClicked(_ sender: Any) {
        let forgotPassController = UIViewController.getFromStoryboard(withId: "ForgotPasswordViewController")
        navigationController?.pushViewController(forgotPassController!, animated: true)
    }
    
    @IBAction func loginClicked(_ sender: Any) {
        Auth.auth().signIn(withEmail: "1@test.com", password: "111111") { (result, error) in
            if error == nil {
                let user = result?.user
                print(user?.email)
            } else {
                print ("Ошибка логина!")
            }
        }
    }
    
    // MARK: - Navigation

}
