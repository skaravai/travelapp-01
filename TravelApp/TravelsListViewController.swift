import UIKit
import RealmSwift

class TravelsListViewController: UIViewController {
    
    var travels: [RealmTravel] = []
    
    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noCountryLabel: UILabel!
    @IBOutlet weak var travelSearch: UISearchBar!
    
    //MARK: - Actions
    
    @IBAction func addClicked(_ sender: Any) {
        let alertController = UIAlertController(title: "Введите название страны", message: nil, preferredStyle: .alert)
        
        // ADD ACTIONS HANDLER
        let createAction = UIAlertAction(title: "Создать", style: .default) { (_) in
            
            let countryTextField = alertController.textFields![0] as UITextField
            let descriptionTextField = alertController.textFields![1] as UITextField
            
            let travel = RealmTravel() //создаем объект класса Travel
            if let country = countryTextField.text { //проверяем введены ли данные в строку со страной
                travel.name = country //записываем в переменную name класса Travel значение введенное в алерте
            }
            if let description = descriptionTextField.text { //проверяем введены ли данные в строку с описанием
                travel.desc = description // записываем в переменную desc класса Travel данные из алерта
            }
            
           // DatabaseManager.instance.saveToDatabase(travels: [travel])
//            DatabaseManager.instance.save([travel])
            DatabaseManager.instance.saveObjects([travel], types: [self.travels])
            
            self.travels.append(travel) //записываем данные из переменной travel типа Travel в массив
            self.tableView.reloadData() //перезагружаем данные в таблице
            
        }
        
        createAction.isEnabled = true
        alertController.addAction(createAction)
        
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel) { (_) in
            // do something
        }
        
        alertController.addAction(cancelAction)
        
        // ADD TEXT FIELDS
        alertController.addTextField { (textField) in
            textField.placeholder = "Страна"
        }
        alertController.addTextField { (textField) in
            textField.placeholder = "Описание"
            
            // enable login button when password is entered
            NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: textField, queue: OperationQueue.main) { (notification) in
                createAction.isEnabled = textField.text != ""
            }
            
            if self.travels.count != 0 {
                self.noCountryLabel.isHidden = true}
        }
        // PRESENT
        present(alertController, animated: true)
        
    }
    
    @IBAction func addButtonClicked(_ sender: UIBarButtonItem) {
        
    }
    
    //MARK: - Lifecycle
    //экран показан, но без верстки
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        travels = DatabaseManager.instance.getObjects(classType: RealmTravel.self)
    
        // проверяем пустой ли массив, если нет, то убираем Label
        if self.travels.count != 0 {
            self.noCountryLabel.isHidden = true}
    }
    
    //почти показали экран
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    //показали и уже есть верстка на экране
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    //срабатывает за долю секунды до закрытия
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //срабатывает сразу после того, как экран исчез
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //срабатывает, когда экран освобождается из памяти (полностью удален)
    deinit {
        
    }
    
    //
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    //срабатывает при перевороте экрана
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    //срабатывает при превышению лимита использования оперативной памяти
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension TravelsListViewController: UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return travels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! TravelCell
        
        cell.travelView.layer.shadowColor = UIColor.gray.cgColor
        cell.travelView.layer.shadowOpacity = 1
        cell.travelView.layer.shadowOffset = .zero
        cell.travelView.layer.shadowRadius = 5
        cell.travelView.layer.shouldRasterize = true
        cell.travelView.layer.rasterizationScale = UIScreen.main.scale
        
        let travel = travels[indexPath.row]
        
        cell.travelNameLabel.text = travel.name
        
        cell.travelSubtitleLabel.text = travel.desc
        
        cell.selectionStyle = .none //отключить подсветку ячейки при тапе по ней
        
        switch travel.averageRate() {
        case 1:
            cell.star1.isHighlighted = true
            break
        case 2:
            cell.star1.isHighlighted = true
            cell.star2.isHighlighted = true
            break
        case 3:
            cell.star1.isHighlighted = true
            cell.star2.isHighlighted = true
            cell.star3.isHighlighted = true
            break
        case 4:
            cell.star1.isHighlighted = true
            cell.star2.isHighlighted = true
            cell.star3.isHighlighted = true
            cell.star4.isHighlighted = true
            break
        case 5:
            cell.star1.isHighlighted = true
            cell.star2.isHighlighted = true
            cell.star3.isHighlighted = true
            cell.star4.isHighlighted = true
            cell.star5.isHighlighted = true
            break
        default:
            cell.star1.isHighlighted = false
            cell.star2.isHighlighted = false
            cell.star3.isHighlighted = false
            cell.star4.isHighlighted = false
            cell.star5.isHighlighted = false
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let stopVC = UIViewController.getFromStoryboard(withId: "StopsListViewController") as! StopsListViewController
        
        stopVC.realmTravel = travels[indexPath.row]
        
        navigationController?.pushViewController(stopVC, animated: true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            travels = DatabaseManager.instance.getObjects(classType: RealmTravel.self)
            tableView.reloadData()
            return
        }

        travels = travels.filter({ realmTravel -> Bool in
            realmTravel.name.lowercased().contains(searchText.lowercased())
    })

        tableView.reloadData()
    }
    
    // удаление выбранной ячейки свайпом слева-направо
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete {
            let travel = travels[indexPath.row]
            DatabaseManager.instance.deleteObject(travel)
//            DatabaseManager.instance.deleteTravelInDatabase(travels[indexPath.row])
            travels.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
        }
    }
}
// !!! Если в таблице не отображаются ячейки, то первым делом нужно проверить установлен ли delegate и dataSource !!!
