import UIKit
import MapKit
protocol CreateStopViewControllerDelegate {
    func createStopControllerDidCreateStop (_ stop: RealmStop)
}

class CreateStopViewController: UIViewController {

    var stop = RealmStop() // создали переменную stop
    var delegate: CreateStopViewControllerDelegate?
    var delegate1: StopsListViewController?
    var stopDidCreateClosure: ((RealmStop) -> Void)?
    
    //MARK: - Outlets
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var moneySpendLabel: UITextField!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var descriptionTextField: UIScrollView!
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UITextView!
    
    //MARK: - Actions
    @IBAction func mapButtonClicked(_ sender: Any) {
        let minsk = MKPointAnnotation() //создали переменную класса MKPointAnnotation
        minsk.coordinate = CLLocationCoordinate2D (latitude: 53.925, longitude: 27.508)
        
        let moscow = MKPointAnnotation() //создали переменную класса MKPointAnnotation
        moscow.coordinate = CLLocationCoordinate2D (latitude: 55.668, longitude: 37.689)
        
        let mapVC = UIViewController.getFromStoryboard(withId: "MapViewController") as! MapViewController // достаем по ID MapViewController и приводим его к типу VC
        mapVC.array = [minsk, moscow]
        mapVC.delegate = self // классическая реализация делегатов
        navigationController?.pushViewController(mapVC, animated: true) // открыть VC c анимацией
    }
    
    @IBAction func saveButtonClicked(_ sender: Any) {
        if let text = nameTextField.text {
            stop.name = text
        }
        if let loc = locationTextField.text {
            stop.location = loc
        }
        if let description = descriptionLabel.text {
            stop.desc = description
        }
        //stop.printAll()
     //   delegate?.createStopControllerDidCreateStop(stop) //по нажатию на save  я своему делегату передаю значения
        stopDidCreateClosure?(stop)
        navigationController?.popViewController(animated: true)
        delegate1?.tableView2.reloadData()
    }
    
    @IBAction func ratingStepper(_ sender: UIStepper) {
        stop.rating = Int(sender.value) ?? 0
        ratingLabel.text = String(stop.rating)
    }
    
    @IBAction func transportTypeSegmentedController(_ sender: UISegmentedControl) {
            if sender.selectedSegmentIndex == 0 {
                stop.transportType = .plane
            } else if sender.selectedSegmentIndex == 1 {
                stop.transportType = .train }
            else if sender.selectedSegmentIndex == 2 {
                stop.transportType = .auto
            }
    }
    
    @IBAction func spendMoneyClicked(_ sender: Any) {
        let spendMoneyController = UIViewController.getFromStoryboard(withId: "SpendMoneyViewController")
        as? SpendMoneyViewController
        spendMoneyController?.delegate = self
        navigationController?.pushViewController(spendMoneyController!, animated: true)
    }
    
    @IBAction func cancelButtonClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        
        //редактирование данных у созданной ранее остановке, передаем указанные ранее значения на экран CreateStopVievController
        nameTextField.text = stop.name
        moneySpendLabel.text = stop.spendMoney
        ratingLabel.text = String(stop.rating)
        currencyLabel.text = stop.currency
        descriptionLabel.text = stop.desc
        locationTextField.text = stop.location
       // transportTypeSegmentedController(UISegmentedControl) ==
    
    }
    
    func userSpentMoney (_ moneyCount: String) {
        moneySpendLabel.text = moneyCount
        if let summ = moneySpendLabel.text {
            stop.spendMoney = summ
        }
    }
    
    func changeSelectedCurrency (_ currency: String) {
        currencyLabel.text = currency
        stop.currency = currency
    }
}

extension CreateStopViewController: MapViewControllerDelegate {
    func mapControllerDidSelectPoint(_ point: MKPointAnnotation) {
    locationTextField.text = "\(point.coordinate.latitude)  \(point.coordinate.longitude)"
    }
}
