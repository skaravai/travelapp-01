//
//  CreateAccountViewController.swift
//  TravelApp
//
//  Created by Karavai on 20/04/2019.
//  Copyright © 2019 Karavai. All rights reserved.
//

import UIKit
import Firebase

class CreateAccountViewController: UIViewController {
    
    
    @IBOutlet weak var emailLabel: UITextField!
    
    @IBOutlet weak var passLabel: UITextField!
    
    @IBOutlet weak var confirmPassLabel: UITextField!
    
    @IBOutlet weak var passSeparator: UIView!
    
    @IBOutlet weak var confirmPassSeparator: UIView!
    
    
    @IBAction func backClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func createClicked(_ sender: Any) {
        Auth.auth().createUser(withEmail: "\(passLabel.text)", password: "\(passLabel.text)") { (result, error) in
            if error == nil {
                print(result?.user.email)
            } else {
                print("Не смогли зарегистрировать")
            }
            
//            if self.passLabel.text != self.confirmPassLabel.text {
//                self.passLabel.text = ""
//                self.confirmPassLabel.text = ""
//                
//                self.passSeparator.backgroundColor = .red
//                
//                self.confirmPassLabel.backgroundColor = .red
//                print("Пароли не совпадают")
//            }
            
        }
        
    }
    
}
    




